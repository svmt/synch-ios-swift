//
//  ViewController.swift
//  synchSDKPlayers
//
//  Created by User on 7/7/21.
//

import UIKit
import AVKit
import SynchSDK
import CallKit

class ViewController: UIViewController, CXCallObserverDelegate {
    @IBOutlet weak var playControlStack: UIStackView!
    @IBOutlet weak var seekControlStack: UIStackView!
    @IBOutlet weak var roomInfoStack: UIStackView!
    @IBOutlet weak var clientsLabel: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var deltaLabel: UILabel!
    @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var syncOnButton: UIButton!
    @IBOutlet weak var syncOffButton: UIButton!
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?

    var room: String!
    var accessToken: String! = ""
    var streamURLString: String!
    var userName: String!
    var syncSDK: SyncSDK?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(accessToken != nil && !accessToken.isEmpty)
        configPlayer()
        setUpSynch()
        pauseButton.isEnabled = false
        syncOffButton.isHidden = true
        setupObservers()
    }

    private func setupObservers() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)   
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    func setUpSynch() {
        do {
            syncSDK = try? SyncSDK(accessToken: accessToken, userName: userName)
            syncSDK?.attachListener(self)
            syncSDK?.syncRateFlow = .threeFloatDigits
        } catch {
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    func startSync() {
        syncSDK?.startSynchronize()
    }
    
    func stopSync() {
        syncSDK?.stopSynchronize()
    }
    
    func configPlayer() {
        guard let streamUrl = URL(string: streamURLString ) else {
            return
        }

        let asset = AVAsset(url: streamUrl)
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        playerItem.audioTimePitchAlgorithm = .varispeed
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = videoContainer.bounds //bounds of the view in which AVPlayer should be displayed
        playerLayer?.videoGravity = .resizeAspect
        videoContainer.layer.addSublayer(playerLayer!)
        player?.prepareForInterfaceBuilder()
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true);
    }

    @IBAction func playAction(_ sender: UIButton) {
        player?.play()
        syncSDK?.playerPlay()
        sender.isEnabled = false
        pauseButton.isEnabled = true
    }

    @IBAction func pauseAction(_ sender: UIButton) {
        player?.pause()
        syncSDK?.playerPause()
        playButton.isEnabled = true
        sender.isEnabled = false
    }
    
    @IBAction func startSynch(_ sender: UIButton) {
        startSync()
        sender.isHidden = true
        syncOffButton.isHidden = false
    }

    @IBAction func stopSynch(_ sender: UIButton) {
        stopSync()
        sender.isHidden = true
        syncOnButton.isHidden = false
    }

    @IBAction func seekBack(_ sender: UIButton) {
        let time = player?.currentTime()
        let newTime = CMTime(seconds: time!.seconds - 30, preferredTimescale: 1)
        player?.seek(to: newTime)
        syncSDK?.playerSeek(position: Int(newTime.seconds * 1000))
    }
    @IBAction func seekForward(_ sender: UIButton) {
        let time = player?.currentTime()
        let newTime = CMTime(seconds: time!.seconds - 30, preferredTimescale: 1)
        player?.seek(to: newTime)
        syncSDK?.playerSeek(position: Int(newTime.seconds * 1000))
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
       super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
        }) { [weak self] (context) in
            self?.videoContainer.frame.size = size
            self?.playerLayer?.frame.size = size
        }
    }

    // MARK: Restore session after call or after background
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.hasEnded {
            callEnded()
        } else {
            callStarted()
        }
    }

    @objc func appMovedToBackground() {
        disconnectFlow()
    }

    @objc func appDidBecomeActive() {
        reconnectFlow()
    }

    func callStarted() {
        disconnectFlow()
    }

    func callEnded() {
        reconnectFlow()
    }

    func disconnectFlow(){
        syncSDK?.stopSynchronize()
    }

    func reconnectFlow() {
        syncSDK?.startSynchronize()
    }
    
    
}

extension ViewController: SynchListener {
    func onClientList(clientList: [Client]) {
        clientsLabel.text = ""
        var i: Int = 0
        for client in clientList {
            clientsLabel.text! += "\(client.name)" + (clientList.count > i+1 ? "\n" : "")
            i += 1
        }
        print(clientList)
    }
    
    func onSetPlaybackRate(rate: Float) {
        player?.rate = rate
    }
    
    func onPlaybackFromPosition(position: Int, participantId: String) {
        player?.seek(to: CMTime(seconds: Double(position)/1000, preferredTimescale: 1))
    }
    
    func onPause(participantId: String) {
        playButton.isEnabled = true
        pauseButton.isEnabled = false
        player?.pause()
    }
    
    func onResumePlay(participantId: String) {
        playButton.isEnabled = false
        pauseButton.isEnabled = true
        player?.play()
    }
    
    func onGetPlayerPosition() -> Int {
        if let currentDate = player?.currentItem?.currentDate() {
            let timeInterval = currentDate.timeIntervalSince1970
            return Int(timeInterval * 1000)
        } else {//for VOD: currentTime()
            let currentTime = player?.currentTime()
            let timeInterval = currentTime?.seconds
            return Int((timeInterval ?? 0) * 1000)
        }
    }
    
    func onGetPlaybackRate() -> Float {
        return player!.rate
    }
    
    func onSyncInfo(accuracy: Float, delta: Int) {
        print("accuracy: ", accuracy, "delta: ", delta)
        accuracyLabel.text = "Accuracy: \(accuracy)"
        deltaLabel.text = "Delta: \(delta)"
    }
    var currentLantency: Double? {
        guard let plr = player, let timeRange = plr.currentItem?.seekableTimeRanges.last?.timeRangeValue else { return nil }
        let start = timeRange.start.seconds
        let totalDuration = timeRange.duration.seconds
        let currentTime = plr.currentTime().seconds
        let secondsBehindLive = currentTime - totalDuration - start
        print("start-> ",start ,"\ncurrentTime-> ",currentTime, "\ntotalDuration-> ", totalDuration)
        return secondsBehindLive
    }
    
}

extension ViewController: SyncLiveEdgeListener {
    func onGetLiveEdgePosition() -> Int? {
        guard let edge = currentLantency else { return nil }
        return Int(edge * 1000.0)
    }
}
