//
//  LoginViewController.swift
//  ExampleSynchSDK
//
//  Created by Anton Umnitsyn on 04.08.2021.
//

import UIKit


enum UserDefaultsKey {
    static let userName = "userName"
    static let streamLink = "streamLink"
    static let token = "token"
}

class LoginViewController: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    var streamLink = "https://demo-app.sceenic.co/football.m3u8"
    // MARK: Add your own method to get access token
    let accessToken = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        setFieldsUI()
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(closeKeyboard))
        view.addGestureRecognizer(tapRecogniser)
        setTextFields()
    }

    private func setFieldsUI() {
        let fieldsArray = [
            userNameTextField,
        ]
        for field in fieldsArray {
            field?.layer.borderColor = UIColor.systemBlue.cgColor
            field?.layer.borderWidth = 0.5
            field?.layer.cornerRadius = 5.0
        }
    }

    /// Set textfields text to stored data form UserDefaults
    private func setTextFields() {
        guard let groupID = UserDefaults.standard.string(forKey: UserDefaultsKey.userName),
              let streamLinkStored = UserDefaults.standard.string(forKey: UserDefaultsKey.streamLink)
        else { return }
        userNameTextField.text = groupID
        streamLink = streamLinkStored
    }


    /// Store text fields data in UserDefaults
    private func storeCredentials() {
        UserDefaults.standard.setValue(userNameTextField.text, forKey: UserDefaultsKey.userName)
        UserDefaults.standard.setValue(streamLink, forKey: UserDefaultsKey.streamLink)
    }


    @objc private func closeKeyboard() {
        view.endEditing(true)
    }

    @IBAction func loginAction(_ sender: Any) {
        if userNameTextField.text!.isEmpty {
            let alert = UIAlertController(title: "Mandatory fields", message: "Fields can't be empty!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        }
        var vc: ViewController
        if #available(iOS 13.0, *) {
            vc = self.storyboard?.instantiateViewController(identifier: "mainView") as! ViewController
        } else {
            vc = ViewController.init()
        }
        vc.accessToken = accessToken
        vc.streamURLString = streamLink
        vc.userName = userNameTextField.text
        storeCredentials()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
